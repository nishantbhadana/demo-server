import UserSchema from "../models/user";
import bcrypt from "bcrypt";
const saltRounds = 10;

function createUser(req, res) {
  let returnObj = {
    success: false,
    message: "User Already Exist`s",
    data: {}
  };

  UserSchema.findOne({ email: req.body.Email })
    .then(foundUser => {
      if (foundUser != null) {
        res.send(returnObj);
      } else {
        bcrypt.hash(req.body.password, saltRounds).then(hashPassword => {
          const newUser = new UserSchema({
            name: req.body.name,
            phone: req.body.phone,
            email: req.body.email,
            userType: req.body.userType,
            interests: req.body.interests,
            password: hashPassword
          });
          newUser.save().then(savedUser => {
            returnObj.success = true;
            returnObj.message = "Users Registered Successfully!";
            returnObj.data = savedUser;
            res.send(returnObj);
          });
        });
      }
    })
    .catch(error => res.send(returnObj));
}

function getUserProfile(req, res) {
  let returnObj = {
    success: false,
    message: "Unable to Find User",
    data: {}
  };

  let userInterestArr = [];
  let _id = req.headers._id;
  UserSchema.findOne({ _id: _id })
    .then(foundUser => {
      if (foundUser.userType == "student") {
        userInterestArr = foundUser.interests;

        UserSchema.find({
          userType: "mentor",
          interests: { $in: userInterestArr }
        })
          .then(foundMentors => {
            let finalResponse = {
              ...foundUser._doc,
              mentors: foundMentors
            };
            returnObj.message = "User Found";
            returnObj.success = true;
            returnObj.data = finalResponse;
            res.send(returnObj);
          })
          .catch(error => {
            res.send(returnObj);
          });
      } else {
        returnObj.success = true;
        returnObj.message = "User Found";
        returnObj.data = foundUser;
        res.send(returnObj);
      }
    })
    .catch(error => res.send(returnObj));
}

export default {
  createUser,
  getUserProfile
};
