import UserSchema from "../models/user";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import config from "../../config/env";
const saltRounds = 10;

function login(req, res) {
  let returnObj = {
    success: false,
    message: "User auth failed",
    jwtAccessToken: null,
    data: {}
  };
  let jwtAccessToken = null;
  UserSchema.findOne({ email: req.body.email }, "+password")
    .then(foundUser => {
      if (foundUser != null) {
        bcrypt.compare(req.body.password, foundUser.password, (err, result) => {
          if (result) {
            jwtAccessToken = jwt.sign(foundUser, config.jwtSecret);
            returnObj.success = true;
            returnObj.message = "Authentication Successful";
            delete foundUser.password;
            returnObj.data = foundUser;
            returnObj.jwtAccessToken = `JWT ${jwtAccessToken}`;
            res.send(returnObj);
          } else {
            res.send(returnObj);
          }
        });
      }
    })
    .catch(error => {
      returnObj.success = false;
      returnObj.message = error;
      res.send(returnObj);
    });
}

export default {
  login
};
