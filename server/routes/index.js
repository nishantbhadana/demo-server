import express from "express";
import authRoutes from "./auth";
import webRoutes from "./web";

//import csvToJson from './csvToJson';

const router = express.Router();

/** GET /health-check - Check service health */
router.get("/health-check", (req, res) => res.send("OK"));

router.get("/", (req, res) => res.send("It`s Working"));

// mount auth routes at /auth
router.use("/auth", authRoutes);

// mount Front-End routes to /web
router.use("/web", webRoutes);

export default router;
