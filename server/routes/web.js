import express from "express";
import userCtrl from "../controllers/user";

const router = express.Router();

router.post("/register", userCtrl.createUser);
router.get("/getUserProfile", userCtrl.getUserProfile);

export default router;
