import mongoose from "mongoose";

var Schema = mongoose.Schema;

const UserSchema = new mongoose.Schema({
  name: { type: String, default: null },
  email: { type: String, default: null, unique: true },
  phone: { type: String, default: null },
  userType: { type: String, default: "student" },
  password: { type: String, default: null, select: false },
  interests: [{ type: String, default: null }]
});

export default mongoose.model("User", UserSchema);
