export default {
  env: "production",
  jwtSecret: "0a6b944d-d2fb-46fc-a85e-0295c986cd9f",
  db: "mongodb://localhost:27017/test-production",
  port: 6001,
  passportOptions: {
    session: false
  }
};
